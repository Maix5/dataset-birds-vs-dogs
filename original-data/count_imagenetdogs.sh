#!/bin/bash

>imagenetdogs_images-count.csv

i=1

for x in $(ls ImageNetDogs/data/Images/)
do
    echo "$i.$(echo $x | sed 's/n.........*-//g'), $(ls ImageNetDogs/data/Images/$x | wc -l)" >> imagenetdogs_images-count.csv
    i=$((i+1))
done
