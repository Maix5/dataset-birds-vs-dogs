#!/bin/bash

>CUB-200-2011_images-count.csv

for x in $(ls CUB-200-2011/data/images/)
do
    echo "$x, $(ls CUB-200-2011/data/images/$x | wc -l)" >> CUB-200-2011_images-count.csv
done
