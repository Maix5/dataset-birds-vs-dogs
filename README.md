# Dataset "Birds vs Dogs". #


### What is this repository for? ###

* This repository contains dataset of images (birds and dogs).
* This is not the original images:
    * Images of birds are taken from: "[The Caltech-UCSD Birds-200-2011 Dataset](http://www.vision.caltech.edu/visipedia/CUB-200-2011.html)". [CITATION DETAILS](https://gitlab.com/Maix5/dataset-birds-vs-dogs/blob/master/README.md#the-caltech-ucsd-birds-200-2011-dataset).
    * Images of dogs are taken from: "[Novel Dataset for Fine-Grained Image Categorization](http://vision.stanford.edu/aditya86/ImageNetDogs/)". [CITATION DETAILS](https://gitlab.com/Maix5/dataset-birds-vs-dogs/blob/master/README.md#novel-dataset-for-fine-grained-image-categorization).
    * Skeches are taken from: "[How Do Humans Sketch Objects?](http://cybertron.cg.tu-berlin.de/eitz/projects/classifysketch/)". [CITATION DETAILS](https://gitlab.com/Maix5/dataset-birds-vs-dogs/blob/master/README.md#how-do-humans-sketch-objects).
* Version 1.0


### Citations ###

#### The Caltech-UCSD Birds-200-2011 Dataset ####

```
@techreport{WahCUB_200_2011,
    Title = {{The Caltech-UCSD Birds-200-2011 Dataset}},
    Author = {Wah, C. and Branson, S. and Welinder, P. and Perona, P. and Belongie, S.},
    Year = {2011}
    Institution = {California Institute of Technology},
    Number = {CNS-TR-2011-001}
}
```

#### Novel Dataset for Fine-Grained Image Categorization ####

```
@inproceedings{KhoslaYaoJayadevaprakashFeiFei_FGVC2011,
    author = "Aditya Khosla and Nityananda Jayadevaprakash and Bangpeng Yao and Li Fei-Fei",
    title = "Novel Dataset for Fine-Grained Image Categorization",
    booktitle = "First Workshop on Fine-Grained Visual Categorization, IEEE Conference on Computer Vision and Pattern Recognition",
    year = "2011",
    month = "June",
    address = "Colorado Springs, CO",
}
```

#### How Do Humans Sketch Objects? ####

```
@article{eitz2012hdhso,
    author={Eitz, Mathias and Hays, James and Alexa, Marc},
    title={How Do Humans Sketch Objects?},
    journal={ACM Trans. Graph. (Proc. SIGGRAPH)},
    year={2012},
    volume={31},
    number={4},
    pages = {44:1--44:10}
}
```


#### Repository avatar piture ####

Image was taken from here: http://taildom.com/blog/videos/bird-loves-her-dog/


#### Dependencies ####

##### Make #####
This ustility for directing compilation.

This command instals the "Make" dependencies (for Desian OS branches (examp.: Debian, Ubuntu, ...), those which uses `apt-get` as package manager.  
`sudo apt-get install make`

##### "Image Magick" softwre #####
Used for image conversions and transformations ([link](https://www.imagemagick.org/script/index.php)).

This command instals the "Image Magick" dependencies (for Desian OS branches (examp.: Debian, Ubuntu, ...), those which uses `apt-get` as package manager.  
`sudo apt-get install imagemagick`

##### Unzip #####
Unarchives zip archives. Used for one data set's images extraction.

This command instals the "Unzip" dependencies (for Desian OS branches (examp.: Debian, Ubuntu, ...), those which uses `apt-get` as package manager.   
`sudo apt-get install unzip`


#### Summary of set up  ####

First make sure to have all dependencies described in [depencencies-list](https://gitlab.com/Maix5/dataset-birds-vs-dogs/blob/master/README.md#dependencies).

It is suggested to use "Makefile" via `make` utility for setting up final dataset.

Data set is being resized accordint to these "Makefile" parameters:

```
CROP_WIDTH=400
CROP_HEIGHT=400
```
Croping is perfomed first for enhansing dtectable objects in original imdages.  

```
IMAGE_WIDTH=32
IMAGE_HEIGHT=32
```
These parameters suggest final size of the images after using `make resize` command.  

Commands worth using are:
* `make dow-all` - downloads original images from original sources (**takes a while**).
* `make proccess-all` - unarchives all downloaded files, and deletes archives.
* `make resize` - copies original images in **data/normal** directory, crops and resizes images.
* `make clean-all` - deletes *original* (not transformed images in "data" directory) downloaded data. Not suggest using this, because dowloading data usually takes long time.

Commands used for adding transfomations to the original images:
* `make blur` - adds 3 levels of blur (0, 1, 2, 3). 0 - no blur.
* `make motion-blur` - adds 3 levels of motion blur (0, 4, 8, 12). 0 - no motion blur.
* `make gaussian-noise` - adds 3 levels of gaussian noise (0, 1, 2, 3). 0 - no noise.
* `make combine-all` - adds 3 levels of transformations (mentioned before) combined (0, 1, 2, 3). 0 - no transformations.
* `make clean` - deletes all **transformed** images (original images are left intact.

P.S. others commands are probably not that usefull.
P.P.S. Commands for trassformation have equivalent command to **deleting** transformed images they start with "clean-<\transformation_word>".

If one wants to set up everything with one command (but it will take a while, becase scripts are not designed to work in parallel. All proccess run on one CPU core) use the following:
`make setup-all`
