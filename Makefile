###################################################
# Recipes for this dataset's preprocessed images. #
###################################################

ORIG_BIRDS_IMAGE_DIR=original-data/CUB-200-2011/data/images/
ORIG_DOGS_IMAGE_DIR=original-data/ImageNetDogs/data/Images/


# Root of data directory.
CURRENT_DATA_DIR=data


# Subdirectories for storring normal/transformed directories.
CURRENT_NORMAL_DATA_DIR=normal
CURRENT_ROTATION_ALL_DATA_DIR=rotation_all
CURRENT_RESIZE_DATA_DIR=resize

CURRENT_GAUSSIAN_NOISE_DATA_DIR=gaussian-noise
CURRENT_BLUR_DATA_DIR=blur
CURRENT_MOTION_BLUR_DATA_DIR=motion-blur
CURRENT_ROTATION_DATA_DIR=rotation
CURRENT_COLOR_DATA_DIR=color
CURRENT_ROLL_DATA_DIR=roll
CURRENT_COMBINE_ALL_DATA_DIR=combine-all


###############################################################################
# Root data setup:
###############################################################################

.PHONY: setup-root
setup-root : dow-all proccess-all normal-birds normal-dogs resize


.PHONY: normal-birds
normal-birds :
	scripts/make-normal-birds-images.sh\
		$(ORIG_BIRDS_IMAGE_DIR) $(ORIG_DOGS_IMAGE_DIR)\
		$(CURRENT_DATA_DIR)\
		$(CURRENT_NORMAL_DATA_DIR)

.PHONY: normal-dogs
normal-dogs :
	scripts/make-normal-dogs-images.sh\
		$(ORIG_BIRDS_IMAGE_DIR) $(ORIG_DOGS_IMAGE_DIR)\
		$(CURRENT_DATA_DIR)\
		$(CURRENT_NORMAL_DATA_DIR)



ROTATION_ANGLE_MIN=0
ROTATION_ANGLE_MAX=359
ROTATION_ANGLE_INCREMENT=90

.PHONY: rotate
rotate :
	scripts/rotate-all-images.sh\
		"$(CURRENT_DATA_DIR)/$(CURRENT_NORMAL_DATA_DIR)"\
		$(ROTATION_ANGLE_MIN)\
		$(ROTATION_ANGLE_MAX)\
		$(ROTATION_ANGLE_INCREMENT)\
		"$(CURRENT_DATA_DIR)/$(CURRENT_ROTATION_ALL_DATA_DIR)"



CROP_WIDTH=400
CROP_HEIGHT=400

IMAGE_WIDTH=64
IMAGE_HEIGHT=64

.PHONY: resize
resize :
	scripts/resize-all-images.sh\
		"$(CURRENT_DATA_DIR)/$(CURRENT_NORMAL_DATA_DIR)"\
		$(CROP_WIDTH) $(CROP_HEIGHT)\
		$(IMAGE_WIDTH) $(IMAGE_HEIGHT)\
		"$(CURRENT_DATA_DIR)/$(CURRENT_RESIZE_DATA_DIR)"


###############################################################################
# Transforamtions:
###############################################################################

.PHONY: gaussian-noise
gaussian-noise :
	scripts/transformation-gaussian-noise.sh\
		$(CURRENT_DATA_DIR)\
		$(CURRENT_GAUSSIAN_NOISE_DATA_DIR)\
		"$(CURRENT_DATA_DIR)/$(CURRENT_RESIZE_DATA_DIR)"

.PHONY: blur
blur :
	scripts/transformation-blur.sh\
		$(CURRENT_DATA_DIR)\
		$(CURRENT_BLUR_DATA_DIR)\
		"$(CURRENT_DATA_DIR)/$(CURRENT_RESIZE_DATA_DIR)"

.PHONY: motion-blur
motion-blur :
	scripts/transformation-motion-blur.sh\
		$(CURRENT_DATA_DIR)\
		$(CURRENT_MOTION_BLUR_DATA_DIR)\
		"$(CURRENT_DATA_DIR)/$(CURRENT_RESIZE_DATA_DIR)"

.PHONY: color
color :
	scripts/transformation-color.sh\
		$(CURRENT_DATA_DIR)\
		$(CURRENT_COLOR_DATA_DIR)\
		"$(CURRENT_DATA_DIR)/$(CURRENT_RESIZE_DATA_DIR)"

.PHONY: roll
roll :
	scripts/transformation-roll.sh\
		$(CURRENT_DATA_DIR)\
		$(CURRENT_ROLL_DATA_DIR)\
		"$(CURRENT_DATA_DIR)/$(CURRENT_RESIZE_DATA_DIR)"\
		$(IMAGE_WIDTH)\
		$(IMAGE_HEIGHT)



.PHONY: rotation
rotation :
	scripts/transformation-rotation.sh\
		$(CURRENT_DATA_DIR)\
		$(CURRENT_ROTATION_DATA_DIR)\
		"$(CURRENT_DATA_DIR)/$(CURRENT_RESIZE_DATA_DIR)"\

# .PHONY: combine-all
# combine-all :
# 	scripts/transformation-combine-all.sh\
# 		$(CURRENT_DATA_DIR) $(CURRENT_COMBINE_ALL_DATA_DIR)




.PHONY: clean
clean: clean-gaussian clean-blur clean-motion-blur clean-color clean-roll clean-rotation
	rm -r "$(CURRENT_DATA_DIR)/$(CURRENT_NORMAL_DATA_DIR)" || true

.PHONY: clean-resize
clean-resize:
	rm -r "$(CURRENT_DATA_DIR)/$(CURRENT_RESIZE_DATA_DIR)" || true

.PHONY: clean-gaussian
clean-gaussian:
	rm -r "$(CURRENT_DATA_DIR)/$(CURRENT_GAUSSIAN_NOISE_DATA_DIR)" || true

.PHONY: clean-blur
clean-blur :
	rm -r "$(CURRENT_DATA_DIR)/$(CURRENT_BLUR_DATA_DIR)" || true

.PHONY: clean-motion-blur
clean-motion-blur :
	rm -r "$(CURRENT_DATA_DIR)/$(CURRENT_MOTION_BLUR_DATA_DIR)" || true

.PHONY: clean-color
clean-color :
	rm -r "$(CURRENT_DATA_DIR)/$(CURRENT_COLOR_DATA_DIR)" || true

.PHONY: clean-roll
clean-roll :
	rm -r "$(CURRENT_DATA_DIR)/$(CURRENT_ROLL_DATA_DIR)" || true


.PHONY: clean-rotation
clean-rotation :
	rm -r "$(CURRENT_DATA_DIR)/$(CURRENT_ROTATION_DATA_DIR)" || true


# .PHONY: clean-combine-all
# clean-combine-all :
# 	rm -r "$(CURRENT_DATA_DIR)/$(CURRENT_COMBINE_ALL_DATA_DIR)" || true



###############################################################################
# Data dowload and extract processes:
###############################################################################

ORIGINAL_BIRDS_DIR=original-data/CUB-200-2011/data
ORIGINAL_DOGS_DIR=original-data/ImageNetDogs/data
ORIGINAL_SKETCH_DIR=original-data/how_do_humans_sketch_objects/data



.PHONY: dow-all
dow-all : dow-orig-birds dow-orig-dogs dow-orig-sketches

.PHONY: proccess-all
proccess-all : proccess-orig-birds proccess-orig-dogs proccess-orig-sketches



.PHONY: dow-orig-birds
dow-orig-birds :
	wget -P $(ORIGINAL_BIRDS_DIR) http://www.vision.caltech.edu/visipedia-data/CUB-200-2011/CUB_200_2011.tgz

.PHONY: proccess-orig-birds
proccess-orig-birds :
	tar -C $(ORIGINAL_BIRDS_DIR) -xzf $(ORIGINAL_BIRDS_DIR)/CUB_200_2011.tgz
	mv $(ORIGINAL_BIRDS_DIR)/CUB_200_2011/* $(ORIGINAL_BIRDS_DIR)
	rm $(ORIGINAL_BIRDS_DIR)/CUB_200_2011.tgz
	rm -d $(ORIGINAL_BIRDS_DIR)/CUB_200_2011



.PHONY: dow-orig-dogs
dow-orig-dogs :
	wget -P $(ORIGINAL_DOGS_DIR) http://vision.stanford.edu/aditya86/ImageNetDogs/images.tar
	wget -P $(ORIGINAL_DOGS_DIR) http://vision.stanford.edu/aditya86/ImageNetDogs/annotation.tar
	wget -P $(ORIGINAL_DOGS_DIR) http://vision.stanford.edu/aditya86/ImageNetDogs/lists.tar

.PHONY: proccess-orig-dogs
proccess-orig-dogs :
	tar -C $(ORIGINAL_DOGS_DIR) -xf $(ORIGINAL_DOGS_DIR)/images.tar
	rm $(ORIGINAL_DOGS_DIR)/images.tar
	tar -C $(ORIGINAL_DOGS_DIR) -xf $(ORIGINAL_DOGS_DIR)/annotation.tar
	rm $(ORIGINAL_DOGS_DIR)/annotation.tar
	tar -C $(ORIGINAL_DOGS_DIR) -xf $(ORIGINAL_DOGS_DIR)/lists.tar
	rm $(ORIGINAL_DOGS_DIR)/lists.tar



.PHONY: dow-orig-sketches
dow-orig-sketches :
	wget -P $(ORIGINAL_SKETCH_DIR) http://cybertron.cg.tu-berlin.de/eitz/projects/classifysketch/sketches_png.zip

.PHONY: proccess-orig-sketches
proccess-orig-sketches :
	unzip -d $(ORIGINAL_SKETCH_DIR) $(ORIGINAL_SKETCH_DIR)/sketches_png.zip
	rm $(ORIGINAL_SKETCH_DIR)/sketches_png.zip



.PHONY: dow-clean
dow-clean:
	rm -r $(ORIGINAL_BIRDS_DIR) || true
	rm -r $(ORIGINAL_DOGS_DIR) || true
	rm -r $(ORIGINAL_SKETCH_DIR) || true
