#!/bin/bash

# script for rotation application
CURRENT_DATA_ROOT_DIR=$1
CURRENT_TRANSFORMATION_DIR=$2
CURRENT_IMAGE_SRC_DIR=$3

# srcDir->destDir->imageName->fileExtension->NULL
function rotationTransformation () {
    local srcDir=$1
    local destDir=$2
    local imageName=$3
    local fileExtension=$4

    local srcImage="${srcDir}/${imageName}.${fileExtension}"
    local intensity=
    for intensity in $(seq 0 90 270)
    do
        echo "${srcImage} - adding rotation: ${intensity}."
        convert ${srcImage}\
                -rotate ${intensity}\
                ${destDir}/${imageName}_${intensity}.${fileExtension}
    done
}


source $(dirname "$0")/transformation-func.sh \
    $CURRENT_DATA_ROOT_DIR\
    $CURRENT_TRANSFORMATION_DIR\
    "rotationTransformation"\
    $CURRENT_IMAGE_SRC_DIR

createDir
