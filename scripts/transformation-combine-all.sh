#!/bin/bash

# script for all transformations application
CURRENT_DATA_ROOT_DIR=$1
CURRENT_TRANSFORMATION_DIR=$2

# srcDir->destDir->imageName->fileExtension->NULL
function allTransformation () {
    local srcDir=$1
    local destDir=$2
    local imageName=$3
    local fileExtension=$4

    local srcImage="${srcDir}/${imageName}.${fileExtension}"
    local intensity=
    for intensity in $(seq 0 3)
    do
        local intensityMotion=$(($intensity*4))
        echo "${srcImage} - adding all trasformations: ${intensity}."
        convert ${srcImage}\
                -blur ${intensity}x${intensity}\
                -motion-blur 0x${intensityMotion}\
                -attenuate ${intensity}.0 +noise gaussian\
                ${destDir}/${imageName}_${intensity}.${fileExtension}
    done
}


source $(dirname "$0")/transformation-func.sh \
    $CURRENT_DATA_ROOT_DIR $CURRENT_TRANSFORMATION_DIR "allTransformation"
createDir
