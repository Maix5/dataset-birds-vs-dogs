#!/bin/bash

# script for blur application
CURRENT_DATA_ROOT_DIR=$1
CURRENT_TRANSFORMATION_DIR=$2
CURRENT_IMAGE_SRC_DIR=$3

IMAGE_WIDTH=$4
IMAGE_HEIGHT=$5

# srcDir->destDir->imageName->fileExtension->NULL
function blurTransformation () {
    local srcDir=$1
    local destDir=$2
    local imageName=$3
    local fileExtension=$4


    echo "${srcImage} - original image."
    cp ${srcImage} "${destDir}/${imageName}.${fileExtension}"


    local srcImage="${srcDir}/${imageName}.${fileExtension}"
    local x_shift=
    for x_shift in $(seq 0 25 25)
    do
        local y_shift=
        for y_shift in $(seq 0 25 25)
        do

            local x=$(echo "scale=0; ${IMAGE_WIDTH} * ${x_shift} / 100" | bc -l)
            local y=$(echo "scale=0; ${IMAGE_HEIGHT} * ${y_shift} / 100" | bc -l)

            echo "${srcImage} - rolling, x: ${x}, x_shift: ${x_shift}."
            echo "${srcImage} - rolling, y: ${y}, y_shift: ${y_shift}."
            echo

            convert ${srcImage}\
                    -roll "+${x}+${y}"\
                    "${destDir}/${imageName}_${x}_${y}.${fileExtension}"
        done
    done
}


source $(dirname "$0")/transformation-func.sh \
    $CURRENT_DATA_ROOT_DIR\
    $CURRENT_TRANSFORMATION_DIR\
    "blurTransformation"\
    $CURRENT_IMAGE_SRC_DIR

createDir
