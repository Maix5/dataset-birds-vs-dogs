#!/bin/bash

# script for images rotation each angle 360 times.
CURRENT_DATA_ROOT_DIR=$1
TRANSFORMATION_TYPE_NAME=$2
TRANSFORMATION_FUNCTION=$3 # srcDir->destDir->imageName->fileExtension->NULL
CURRENT_DATA_DIR=$4 # src images location for the transformation.
# CURRENT_DATA_DIR="$CURRENT_DATA_ROOT_DIR/normal"

TRANSFORMATION_TEST_DATA_DIR="$CURRENT_DATA_ROOT_DIR/$TRANSFORMATION_TYPE_NAME/test"
TRANSFORMATION_TRAIN_DATA_DIR="$CURRENT_DATA_ROOT_DIR/$TRANSFORMATION_TYPE_NAME/train"

function transformAllImages () {
    local srcDir=$1
    local destDir=$2

    local regexExtractNotExtention="^[^\.]+"
    local regexExtractExtention="[^\.]+$"

    local image=
    local dirImange=
    for image in $(ls $srcDir)
    do
        dirImange=$srcDir/$image

        local angle=
        local imageNameNotExtention=$(echo $image | grep -oP $regexExtractNotExtention)
        local imageNameExtention=$(echo $image | grep -oP $regexExtractExtention)

        ($TRANSFORMATION_FUNCTION\
            $srcDir $destDir $imageNameNotExtention $imageNameExtention)
    done
}

function forEachDir () {
    local srcDir=$1
    local outDir=$2

    local dir=
    for dir in $(ls $srcDir)
    do
        mkdir -p "$outDir/$dir"
        transformAllImages "$srcDir/$dir" "$outDir/$dir"
    done
}

function createDir () {
    mkdir -p $TRANSFORMATION_TEST_DATA_DIR
    mkdir -p $TRANSFORMATION_TRAIN_DATA_DIR

    forEachDir\
        "$CURRENT_DATA_DIR/test/birds"\
        "$TRANSFORMATION_TEST_DATA_DIR/birds"
    forEachDir\
        "$CURRENT_DATA_DIR/train/birds"\
        "$TRANSFORMATION_TRAIN_DATA_DIR/birds"

    forEachDir\
        "$CURRENT_DATA_DIR/test/dogs"\
        "$TRANSFORMATION_TEST_DATA_DIR/dogs"
    forEachDir\
        "$CURRENT_DATA_DIR/train/dogs"\
        "$TRANSFORMATION_TRAIN_DATA_DIR/dogs"
}
