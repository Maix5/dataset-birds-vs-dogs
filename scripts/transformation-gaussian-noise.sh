#!/bin/bash

# script for gaussian noise application
CURRENT_DATA_ROOT_DIR=$1
CURRENT_TRANSFORMATION_DIR=$2
CURRENT_IMAGE_SRC_DIR=$3

# srcDir->destDir->imageName->fileExtension->NULL
function noiseTransformation () {
    local srcDir=$1
    local destDir=$2
    local imageName=$3
    local fileExtension=$4

    local srcImage="${srcDir}/${imageName}.${fileExtension}"
    local intensity=
    for intensity in $(seq 0 3)
    do
        echo "${srcImage} - adding noise: ${intensity}."
        convert ${srcImage}\
                -attenuate ${intensity}.0 +noise gaussian\
                ${destDir}/${imageName}_${intensity}.${fileExtension}
    done
}


source $(dirname "$0")/transformation-func.sh \
    $CURRENT_DATA_ROOT_DIR\
    $CURRENT_TRANSFORMATION_DIR\
    "noiseTransformation"\
    $CURRENT_IMAGE_SRC_DIR

createDir
