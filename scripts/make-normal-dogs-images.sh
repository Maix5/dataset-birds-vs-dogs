#!/bin/bash

# functions for selecting unmodyfied dogs data.
ORIG_BIRDS_IMAGE_DIR=$1
ORIG_DOGS_IMAGE_DIR=$2
CURRENT_DATA_DIR=$3
dirType=$4

source $(dirname "$0")/make-func-for-normal-images.sh \
    $ORIG_BIRDS_IMAGE_DIR $ORIG_DOGS_IMAGE_DIR $CURRENT_DATA_DIR $dirType "dog"
make_dogs_dirs
