#!/bin/bash

# script for blur application
CURRENT_DATA_ROOT_DIR=$1
CURRENT_TRANSFORMATION_DIR=$2
CURRENT_IMAGE_SRC_DIR=$3

# srcDir->destDir->imageName->fileExtension->NULL
function blurTransformation () {
    local srcDir=$1
    local destDir=$2
    local imageName=$3
    local fileExtension=$4

    local srcImage="${srcDir}/${imageName}.${fileExtension}"

    echo "${srcImage} - original colors."
    cp ${srcImage} "${destDir}/${imageName}.${fileExtension}"


    echo "${srcImage} - normalizing contrast."
    convert\
        ${srcImage}\
        -normalize\
        "${destDir}/${imageName}_normalized.${fileExtension}"


    echo "${srcImage} - grayscale."
    convert\
        ${srcImage}\
        -colorspace Gray\
        "${destDir}/${imageName}_grayscale.${fileExtension}"


    echo "${srcImage} - negative."
    convert\
        ${srcImage}\
        -level 100%,0\
        "${destDir}/${imageName}_negative.${fileExtension}"


    echo "${srcImage} - sepia."
    convert\
        ${srcImage}\
        -sepia-tone 100%\
        "${destDir}/${imageName}_sepia.${fileExtension}"

}


source $(dirname "$0")/transformation-func.sh \
    $CURRENT_DATA_ROOT_DIR\
    $CURRENT_TRANSFORMATION_DIR\
    "blurTransformation"\
    $CURRENT_IMAGE_SRC_DIR

createDir
