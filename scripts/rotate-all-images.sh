#!/bin/bash

# script for images rotation each angle 360 times.
CURRENT_DATA_DIR=$1

ROTATION_ANGLE_MIN=$2
ROTATION_ANGLE_MAX=$3
ROTATION_ANGLE_INCREMENT=$4

CURRENT_ROTATE_DATA_DIR=$5


function rotateAllImages () {
    local dir=$1
    local dirDestination=$2

    local regexExtractNotExtention="^[^\.]+"
    local regexExtractExtention="[^\.]+$"


    mkdir -p $dirDestination

    local image=
    local dirImange=
    for image in $(ls $dir)
    do
        dirImange=$dir/$image
        local angle=
        for angle in $(seq $ROTATION_ANGLE_MIN $ROTATION_ANGLE_INCREMENT $ROTATION_ANGLE_MAX)
        do
            echo "$dir/$image is being rotated $angle"
            echo "Storing to: $dirDestination"
            echo

            convert\
                $dirImange\
                -alpha set \( +clone -background none -rotate $angle \) \
                -gravity center  -compose Src -composite\
                "$dirDestination/$(echo $image | grep -oP $regexExtractNotExtention)_$angle.$(echo $image | grep -oP $regexExtractExtention)"
        done
    done
}

function forEachDir () {
    local currentDir=$1
    local destinationDir=$2

    local dir=
    for dir in $(ls $currentDir)
    do
        rotateAllImages "$currentDir/$dir" "$destinationDir/$dir"
    done
}

mkdir -p $CURRENT_ROTATE_DATA_DIR

forEachDir\
    "$CURRENT_DATA_DIR/test/birds"\
    "$CURRENT_ROTATE_DATA_DIR/test/birds"
forEachDir\
    "$CURRENT_DATA_DIR/test/dogs"\
    "$CURRENT_ROTATE_DATA_DIR/test/dogs"
forEachDir\
    "$CURRENT_DATA_DIR/train/birds"\
    "$CURRENT_ROTATE_DATA_DIR/train/birds"
forEachDir\
    "$CURRENT_DATA_DIR/train/dogs"\
    "$CURRENT_ROTATE_DATA_DIR/train/dogs"
