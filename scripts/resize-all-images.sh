#!/bin/bash

# script for images rotation each angle 360 times.
CURRENT_DATA_DIR=$1

CROP_WIDTH=$2
CROP_HEIGHT=$3

IMAGE_WIDTH=$4
IMAGE_HEIGHT=$5

CURRENT_RESIZE_DATA_DIR=$6


function resizeAllImages () {
    local dir=$1
    local dirDestination=$2


    local regexExtractNotExtention="^[^\.]+"
    local regexExtractExtention="[^\.]+$"


    mkdir -p $dirDestination

    local image=
    local dirImange=
    for image in $(ls $dir)
    do
        dirImange=$dir/$image
        dirDestinationImage=$dirDestination/$image
        echo "$dir/$image is being resized."
        echo "Storing to: $dirDestination"
        echo

        convert $dirImange\
            -gravity Center\
            -crop "${CROP_WIDTH}x${CROP_HEIGHT}+0+0"\
            -resize "${IMAGE_WIDTH}x${IMAGE_HEIGHT}!"\
            $dirDestinationImage
    done
}

function forEachDir () {
    local currentDir=$1
    local destinationDir=$2

    local dir=
    for dir in $(ls $currentDir)
    do
        resizeAllImages "$currentDir/$dir" "$destinationDir/$dir"
    done
}

mkdir -p $CURRENT_RESIZE_DATA_DIR

forEachDir\
    "$CURRENT_DATA_DIR/test/birds"\
    "$CURRENT_RESIZE_DATA_DIR/test/birds"

forEachDir\
    "$CURRENT_DATA_DIR/test/dogs"\
    "$CURRENT_RESIZE_DATA_DIR/test/dogs"

forEachDir\
    "$CURRENT_DATA_DIR/train/birds"\
    "$CURRENT_RESIZE_DATA_DIR/train/birds"

forEachDir\
    "$CURRENT_DATA_DIR/train/dogs"\
    "$CURRENT_RESIZE_DATA_DIR/train/dogs"
