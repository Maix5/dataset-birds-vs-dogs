#!/bin/bash

# functions for selecting unmodyfied birds and dogs data.

ORIG_BIRDS_IMAGE_DIR=$1
ORIG_DOGS_IMAGE_DIR=$2
CURRENT_DATA_DIR=$3
dirType=$4
labelName=$5

IMAGE_COUNT=0

ORIG_IMAGE_DIR=
COUNT_TRAIN_IMAGES=37
COUT_TEST_IMAGES=4
COUNT_IMAGE_CATEGORIES=120


function select_images () {
    local pictureType=$1
    local srcDirName=$2
    local destDir=$3
    local commandSelect=$4

    local srcDir=$ORIG_IMAGE_DIR/$srcDirName

    local i=$IMAGE_COUNT
    local image=
    for image in $(ls $srcDir | $commandSelect)
    do
        cp\
            $srcDir/$image\
            $destDir/${i}_${pictureType}$(echo "$image" | grep -oP '\..+$')
        i=$((i+1))
    done
}

function make_dirs () {
    local animalType=$1
    local regexToExtractAnimalCategory=$2

    local i=1

    local dir=
    for  dir in $(ls $ORIG_IMAGE_DIR | head -n$COUNT_IMAGE_CATEGORIES)
    do
        local dirTypeCategory=$(echo "$dir" | grep -oP $regexToExtractAnimalCategory)
        local destDirTest="$CURRENT_DATA_DIR/$dirType/test/$animalType/$i.$dirTypeCategory"
        local destDirTrain="$CURRENT_DATA_DIR/$dirType/train/$animalType/$i.$dirTypeCategory"

        mkdir -p $destDirTest
        select_images $labelName $dir $destDirTest "tail -n$COUT_TEST_IMAGES"

        mkdir -p $destDirTrain
        select_images $labelName $dir $destDirTrain "head -n$COUNT_TRAIN_IMAGES"

        i=$((i+1))
    done
}

function make_birds_dirs () {
    ORIG_IMAGE_DIR=$ORIG_BIRDS_IMAGE_DIR
    make_dirs "birds" "[^.]+$"
}

function make_dogs_dirs () {
    ORIG_IMAGE_DIR=$ORIG_DOGS_IMAGE_DIR
    make_dirs "dogs" "[^-]+$"
}
